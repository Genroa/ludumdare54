class_name FishData extends Node

@export var inventorySprite :Texture2D = null
@export var currentOrientation = 0
@export var shape = [
	[0, 0, 0, 0, 0],
	[0, 0, 0, 0, 0],
	[0, 0, 0, 0, 0],
	[0, 0, 0, 0, 0],
	[0, 0, 0, 0, 0],
]
@export var fishSize = 0
@export var fishType:Enum.FISH_TYPE
@export var spriteOrientationOffsets = [
	Vector2(0, 0),
	Vector2(0, 0),
	Vector2(0, 0),
	Vector2(0, 0),
]
@export var last_direction:Vector2

func _ready():
	Events.connect("caught_fish",on_fish_caught)

func on_fish_caught(fish_data):
	if fish_data == self:
		get_parent().queue_free()

func set_last_direction(direction:Vector2):
	last_direction = direction
	
func get_last_direction() -> Vector2:
	return last_direction
