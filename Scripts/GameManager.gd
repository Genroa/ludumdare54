extends Node

var level_root

func _ready():
	Events.connect("start_game",on_game_start)
	Events.connect("finish_game",on_game_finished)
	Events.connect("game_over",on_game_over)

func on_game_start(level_index):
	print("START GAME WITH LEVEL: " + str(level_index))
	Events.show_main_menu.emit(false)
	LevelManager.load_level(level_index)

func on_game_finished():
	Events.pause_game.emit(true)
	print("GAME FINISHED")
	pass
	
func on_game_over():
	Events.pause_game.emit(true)
	print("GAME OVER")
	pass
