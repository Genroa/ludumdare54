extends Node

class_name Level

@onready var tile_map_pressure = $TileMap_pressure
@onready var tile_map_collision = $TileMap_collision

@onready var spaceship = $Spaceship
@onready var levelgoal = $LevelGoal

func _ready():
	LevelManager.set_current_level(self as Level)
	Events.start_level.emit()
	
func get_spaceship_global_position() -> Vector2:
	return spaceship.global_position

