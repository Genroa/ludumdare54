extends Node2D
var baseOffset
var isGaming = false
# Called when the node enters the scene tree for the first time.
func _ready():
	Events.connect("show_main_menu", menuShow)
	baseOffset = $Camera2D2.offset


# Called every frame. 'delta' is the elapsed time since the previous frame.
func _process(delta):

	Utility.ScreenShake($Camera2D2, delta, baseOffset)
	if (isGaming and Input.is_action_pressed("reset")):
		Events.emit_signal("restart_level")
	if (isGaming and Input.is_action_pressed("GiveUp")):
		Events.emit_signal("game_over")
func menuShow(p_show):
	if(p_show):
		isGaming = false
	else:
		isGaming = true
