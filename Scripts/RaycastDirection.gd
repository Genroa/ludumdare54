extends RayCast2D

signal eat_fish(direction:Vector2,eaten_object:Node2D)

@export var directions_to_check: Array[Vector2]
@export var distance = 16

func isEdibleFish(fishNode, edible_fish_type:Enum.FISH_TYPE):
	print("isFish? ", fishNode is Fish)
	if !(fishNode is Fish):
		return false
	print("FishType: ", (fishNode.get_node("FishData") as FishData).fishType)
	return fishNode is Fish && (fishNode.get_node("FishData") as FishData).fishType == edible_fish_type

func CheckEdibleFish(edible_fish_type) -> bool:
	var has_seen_fish = false
	
	for direction in directions_to_check:
		target_position = direction * distance
		force_raycast_update()
		
		if is_colliding():
			print("COLLIDE WITH: " + get_collider().get_parent().name)
			var collidingElement = get_collider().get_parent()
			if isEdibleFish(collidingElement, edible_fish_type):
				has_seen_fish = true
				eat_fish.emit(direction, collidingElement)
				return has_seen_fish
				
	return has_seen_fish
	
# Called when the node enters the scene tree for the first time.
func CheckObstacle(edible_fish_type, direction: Vector2) -> bool:
	var has_obstacle = false
	
	target_position = direction  * distance
	force_raycast_update()
	
	if is_colliding():
		var collidingElement = get_collider().get_parent()
		if isEdibleFish(collidingElement, edible_fish_type):
			has_obstacle = false
			eat_fish.emit(Vector2.ZERO, collidingElement)
		elif collidingElement.name == "Spaceship":
			has_obstacle = false
		else :
			has_obstacle = true
		
	return has_obstacle
