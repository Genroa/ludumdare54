extends Control

@onready var restart_button:Button = $RestartButton

var FirstWarning = true
var SecondWarning = true

var storeTurns = true
var totalTurn = 0

var ObjoNodeRes = load("res://Scenes/UI/ObjectifNode.tscn")
# Called when the node enters the scene tree for the first time.
func _ready():
	Events.connect("spaceship_has_moved", TurnDecrease)
	Events.connect("start_game", OnGameStart)
	Events.connect("start_level", OnStart)
	Events.connect("show_main_menu", OnMenuShow)
	Events.connect("restart_level", OnStart)
	Events.connect("update_inventory", OnInventoryUpdate)



# Called every frame. 'delta' is the elapsed time since the previous frame.
func _process(delta):
	pass

func TurnDecrease(remainingMove):
	if (storeTurns) : 
		storeTurns = false
		totalTurn = remainingMove
	if (remainingMove <= 10 and FirstWarning):
		FirstWarning = false
		$TextureProgressBar.texture_progress.color_ramp = load("res://Fonts/Warning1_Gradient.tres")
		Utility.RequestShake(5.0, 10.0)
	if (remainingMove <= 5 and SecondWarning):
		SecondWarning = false
		$TextureProgressBar.texture_progress.color_ramp = load("res://Fonts/Warning2_Gradient.tres")
		Utility.RequestShake(10.0, 10.0)
	$TextureProgressBar/TurnsCount.text = str(remainingMove)
	$TextureProgressBar.value = float(remainingMove) / float(totalTurn) * 100
	
	
func OnGameStart(_index):
	self.show()
	

func OnMenuShow(p_show):
	if (p_show):
		self.hide()
	else:
		self.show()
		
func OnStart():
	$TextureProgressBar/TurnsCount.add_theme_color_override("font_color", Color.WHITE)
	FirstWarning = true
	SecondWarning = true
	storeTurns = true
	$TextureProgressBar.value = 100
	$TextureProgressBar.texture_progress.color_ramp = load("res://Fonts/Normal_Gradient.tres")
	loadObjectives()
	
func loadObjectives():
	for child in $HBoxContainer/FishObjo.get_children():
		child.queue_free()
		
	var objList = LevelManager.current_level.levelgoal.fish_to_catch
	
	for fish in objList:
		var objoNode = ObjoNodeRes.instantiate() as ObjectifNode
		objoNode.init(fish)
		$HBoxContainer/FishObjo.add_child(objoNode)
	
func OnInventoryUpdate(fishArray):
	var usedArray = []
	for i in range(len(fishArray)):
		usedArray += [false]
	
	var cleared = true
	var objList = $HBoxContainer/FishObjo
	for child in objList.get_children():
		child = child as ObjectifNode
		child.setProgress(false)
		
		var i = 0
		for fish  in fishArray:
			if fish.fishType == child.fishType and not usedArray[i]:
				usedArray[i] = true
				child.setProgress(true)
				break
			i += 1
		if (not child.getProgress()):
			cleared = false
	if (cleared):
		Events.finish_level.emit()


func _on_button_pressed():
	Events.restart_level.emit()
	restart_button.release_focus()
