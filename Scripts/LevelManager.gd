extends Node

var level_root_node:Node2D
var current_level:Level

@export var level_index:int = 1

func _ready():
	Events.connect("load_next_level",on_load_next_level)
	Events.connect("restart_level",on_level_restart)

func set_level_root(level_root):
	level_root_node = level_root
	
func get_level_root() -> Node:
	return level_root_node

func on_level_restart():
	load_level(level_index)

func set_current_level(level:Level):
	print("SET CURRENT LEVEL: " + str(level_index))
	current_level = level
	Events.init_movement.emit()

func get_current_level() -> Level:
	return current_level

func on_load_next_level():
	load_level(level_index+1)

func load_level(index:int):
	print("START GAME WITH LEVEL: " + str(level_index))
	
	if !ResourceLoader.exists("res://Scenes/Levels/level_" + str(index).pad_zeros(2) + ".tscn"):
		Events.finish_game.emit()
		return
	elif current_level != null:
		current_level.queue_free()
	
	level_index = index
	var level_to_load 
	level_to_load = load("res://Scenes/Levels/level_" + str(index).pad_zeros(2) + ".tscn").instantiate() as Node
	level_root_node.add_child(level_to_load)
	set_current_level(level_to_load)
