extends Node2D

@onready var sprite_2d = $Sprite2D

@onready var tilemap: TileMap = $"../CurrentLevel/Level01/TileMap"

@onready var current_level = $"../CurrentLevel"

@onready var audioplayer = $Movement/audio_random_movment

@onready var movement = $Movement

@export var move_number = 20

@onready var shipVisuals = [
	"res://Sprites/TEX_spaceship.png",
	"res://Sprites/TEX_spaceship_p1.png",
	"res://Sprites/TEX_spaceship_p2.png"
]

var is_managing_inventory = false
var is_paused = false
var can_move = true

# Called when the node enters the scene tree for the first time.
func _ready():
	movement.connect("player_has_moved",on_spaceship_move)
	
	Events.connect("player_turn",on_player_turn)
	
	Events.connect("manage_inventory",inventory_signal)

	Events.connect("pause_game",on_game_paused)
	
	Events.spaceship_has_moved.emit(move_number)
	
	Events.update_current_pressure.connect(updateShipVisuals)

func on_game_paused(paused):
	is_paused = paused

func on_player_turn():
	can_move = true

# Called every frame. 'delta' is the elapsed time since the previous frame.
func _process(delta):
	if is_paused:
		return
	
	if is_managing_inventory:
		return
		
	if !can_move:
		return
		
	manage_control()

func inventory_signal(is_managing):
	is_managing_inventory = is_managing

func manage_control():
	if Input.is_action_just_pressed("up"):
		movement.move(Vector2.UP)
	elif Input.is_action_just_pressed("down"):
		movement.move(Vector2.DOWN)
	elif Input.is_action_just_pressed("left"):
		movement.move(Vector2.LEFT)
	elif Input.is_action_just_pressed("right"):
		movement.move(Vector2.RIGHT)
	elif Input.is_action_just_pressed("wait"):
		movement.move(Vector2.ZERO)

func wait_for_next_move():
	can_move = false
	
func _on_area_2d_area_entered(area):
	if area.is_in_group("danger"):
		Events.game_over.emit()
	elif area.get_parent().get_node("FishData"):
		Events.update_current_tile_object.emit(area.get_parent().get_node("FishData"))
	
	
func _on_area_2d_area_exited(area):
	if area.get_parent().get_node("FishData"):
		Events.update_current_tile_object.emit(null)
	
func emit_enemy_turn():
	await get_tree().create_timer(0.1).timeout
	Events.enemy_turn.emit()

func on_spaceship_move():
	emit_enemy_turn()
	audioplayer.play()
	move_number -= 1
	Events.spaceship_has_moved.emit(move_number)
	if move_number <= 0:
		Events.game_over.emit()
		

func updateShipVisuals(newPressure):
	$Sprite2D.texture = load(shipVisuals[newPressure])
