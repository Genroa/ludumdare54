extends Control

const MainMenu = "res://Scenes/Levels/MainMenu.tscn"

# Called when the node enters the scene tree for the first time.
func _ready():
	pass # Replace with function body.


# Called every frame. 'delta' is the elapsed time since the previous frame.
func _process(delta):
	pass


func _on_main_menu_button_pressed():
	get_tree().change_scene_to_file(MainMenu)


func _on_continue_button_pressed():
	pass


func _on_retrybutton_pressed():
	get_tree().reload_current_scene()
