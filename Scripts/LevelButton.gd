class_name LevelButton extends Button

var LevelIndex = 0
# Called when the node enters the scene tree for the first time.
func _ready():
	pass
	
func init(idx):
	LevelIndex = idx
	self.text = Utility.LevelNames[idx] if Utility.LevelNames.has(idx) else ("Level "+str(idx))
	

func _on_pressed():
	Events.start_game.emit(LevelIndex)
	
