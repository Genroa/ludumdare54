extends Node

signal spaceship_has_moved(remainingMove)
signal player_turn
signal enemy_turn
signal manage_inventory(is_managing)
signal update_current_tile_object(object)
signal update_current_pressure(pressure_index)
signal init_movement()

signal caught_fish(fish_data)
signal released_fish(fish_data)
signal update_inventory(fish_data_array)

signal start_game(level_index)
signal pause_game(paused)
signal restart_level
signal finish_level
signal load_next_level
signal start_level
signal finish_game
signal game_over

signal show_main_menu(show)

signal update_position_validity(is_valid)
