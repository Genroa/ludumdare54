extends Node

@export var is_player:bool

var tile_map_pressure:TileMap
var tile_map_collision: TileMap

@onready var sprite_2d = $"../Sprite2D"

var lerp_speed = 10.0

var last_pressure:int

signal player_has_moved
signal target_tile_has_collision

func _ready():
	Events.connect("init_movement",on_init_movement)
	
# Called when the node enters the scene tree for the first time.
func on_init_movement():
	tile_map_pressure = LevelManager.current_level.tile_map_pressure
	tile_map_collision = LevelManager.current_level.tile_map_collision
	
	var current_tile: Vector2i = tile_map_pressure.local_to_map(get_parent().global_position)
	get_parent().global_position = tile_map_pressure.map_to_local(current_tile)
	
	if is_player:
		var tile_data: TileData = tile_map_pressure.get_cell_tile_data(0,current_tile)
		var current_pressure = tile_data.get_custom_data("pressure")
		Events.update_current_pressure.emit(current_pressure)
		last_pressure = current_pressure

func get_target_tile_collision(direction) -> bool:
	var has_collision = false
	var current_tile: Vector2i = tile_map_pressure.local_to_map(get_parent().global_position)
	var target_tile: Vector2i = Vector2i(current_tile.x + direction.x,current_tile.y + direction.y)
	
	var tile_pressure_data: TileData = tile_map_pressure.get_cell_tile_data(0,target_tile)
	var tile_collision_data: TileData = tile_map_collision.get_cell_tile_data(0,target_tile)
	
	if tile_collision_data != null:
		if(tile_collision_data.get_custom_data("collision")) == true:
			has_collision = true
	return has_collision

func _process(delta):
	if tile_map_pressure == null:
		return
	
	if tile_map_collision == null:
		return
	
	if sprite_2d == null:
		return
		
	sprite_2d.global_position = lerp(sprite_2d.global_position,get_parent().global_position,delta*lerp_speed)

func move(direction: Vector2):
	var current_tile: Vector2i = tile_map_pressure.local_to_map(get_parent().global_position)
	var target_tile: Vector2i = Vector2i(current_tile.x + direction.x,current_tile.y + direction.y)
	
	var tile_pressure_data: TileData = tile_map_pressure.get_cell_tile_data(0,target_tile)
	var tile_collision_data: TileData = tile_map_collision.get_cell_tile_data(0,target_tile)
	
	if tile_collision_data != null:
		if(tile_collision_data.get_custom_data("collision")) == true:
			target_tile_has_collision.emit()
			return
		
	get_parent().global_position = tile_map_pressure.map_to_local(target_tile)
	
	if is_player:
		player_has_moved.emit()
		var current_pressure = tile_pressure_data.get_custom_data("pressure")
		if last_pressure != current_pressure:
			Events.update_current_pressure.emit(tile_pressure_data.get_custom_data("pressure"))
			
		last_pressure = current_pressure
	
	if sprite_2d == null:
		return
		
	sprite_2d.global_position  = tile_map_pressure.map_to_local(current_tile)
	
	if direction.x > 0 :
		sprite_2d.set_flip_h(true)
	elif direction.x < 0:
		sprite_2d.set_flip_h(false)
		
	var tween = get_tree().create_tween()
	if direction == Vector2.ZERO:
		tween.tween_property(sprite_2d, "scale", Vector2(1.1,1.1), 0.05)
		tween.tween_interval(0.05)
		tween.tween_property(sprite_2d, "scale", Vector2(1,1), 0.05)
	else:
		tween.tween_property(sprite_2d, "scale", Vector2(1 + direction.x*0.3,1 + direction.y*0.3), 0.05)
		tween.tween_interval(0.05)
		tween.tween_property(sprite_2d, "scale", Vector2(1,1), 0.05)
