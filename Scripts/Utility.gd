extends Node

var clampoffset = 0.3
var magnitude = 0.0
var decay = 5.0
var rng = RandomNumberGenerator.new()

const level_count = 8

@export var FishTexture = {
	Enum.FISH_TYPE.PLANKTON : "res://Sprites/TEX_plankton_mini.png",
	Enum.FISH_TYPE.MANTA : "res://Sprites/TEX_manta_mini.png",
	Enum.FISH_TYPE.JELLYFISH : "res://Sprites/TEX_jellyfish_mini.png",
	Enum.FISH_TYPE.SHARK : "res://Sprites/TEX_manta_mini.png",
	Enum.FISH_TYPE.WHALE : "res://Sprites/TEX_manta_mini.png",
	Enum.FISH_TYPE.STARFISH : "res://Sprites/TEX_starfish_mini.png",
}

@export var LevelNames = {
	1 : "1. Basics of Capturing", # learn capturing
	2 : "2. Almost Close Enough...", # learn limited moves
	3 : "3. Under Pressure", # learn about pressure
	4 : "4. Swirling Cauldron", # learn rotation
	5 : "5. Flying among the Jellyfish", # learn letting fishes go
	6 : "6. Fit Everything In", #something something, see a Manta in action
	7 : "7. Drawing the Predator", # draw a Manta higher to allow to keep it in your inventory
	8 : "8. Pursuit" # draw a Manta higher to allow to keep it in your inventory
}

var isShaking = false

# Called when the node enters the scene tree for the first time.
func _ready():
	pass # Replace with function body.

func RequestShake(p_magnitude = 5.0, p_decay = 10.0):
	magnitude = p_magnitude
	decay = p_decay
	isShaking = true
	
func ScreenShake(Camera, delta, baseoffset = Vector2.ZERO):
	if (magnitude < clampoffset and isShaking):
		magnitude = 0.0
		Camera.offset = baseoffset
		isShaking = false
	if (magnitude != 0):
		Camera.offset = baseoffset + GetRandomOffset()
	
	magnitude = lerp(magnitude, 0.0, decay * delta)
	
func GetRandomOffset() -> Vector2:
	return Vector2(
		rng.randf_range(-magnitude, magnitude),
		rng.randf_range(-magnitude, magnitude)
	)
