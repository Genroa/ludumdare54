extends Fish

class_name Manta

@export var edible_fish_type:Enum.FISH_TYPE

@onready var movement = $Movement
@onready var sprite_2d = $Sprite2D
@onready var ray_cast_2d = $RayCast2D
@onready var fish_data = $FishData

func _ready():
	movement.connect("target_tile_has_collision",_on_target_tile_has_collision)
	ray_cast_2d.connect("eat_fish",_on_eat_fish)
	
	if current_direction.x > 0 :
		sprite_2d.set_flip_h(true)
	elif current_direction.x < 0:
		sprite_2d.set_flip_h(false)
	

func _on_target_tile_has_collision():
	current_direction.x *= -1
	
	if movement.get_target_tile_collision(current_direction):
		return
	
	movement.move(current_direction)
	fish_data.set_last_direction(current_direction)
	
func _on_eat_fish(direction: Vector2,eaten_object:Node2D):
	if direction != Vector2.ZERO:
		movement.move(direction)
		
	print("HAS EATEN FISH")
	await get_tree().create_timer(0.1).timeout
	eaten_object.queue_free()
	
func on_enemy_turn():
	if ray_cast_2d.CheckEdibleFish(edible_fish_type):
		return
		
	if ray_cast_2d.CheckObstacle(edible_fish_type,current_direction):
		current_direction.x *= -1
		
	movement.move(current_direction)
	fish_data.set_last_direction(current_direction)


func _on_area_2d_area_entered(area):
	pass
