extends Control

const MainMenu = "res://Scenes/Levels/game.tscn"
@onready var label = $Label

var nextLevel
# Called when the node enters the scene tree for the first time.
func _ready():
	Events.connect("finish_level",on_level_finished)
	self.hide()

func _on_main_menu_button_pressed():
	get_tree().reload_current_scene()

func on_level_finished():
	self.show()
	nextLevel = "res://Scenes/Levels/level_" + str(LevelManager.level_index+1).pad_zeros(2) + ".tscn"
	if !ResourceLoader.exists(nextLevel):
		label.text = "GAME FINISHED"
		$NextLevel.hide()
		return
	label.text = "LEVEL FINISHED"
	
func _on_next_level_pressed():
	Events.load_next_level.emit()
	self.hide()
