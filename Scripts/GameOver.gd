extends Control
@onready var game_over_sound = $AudioStreamPlayer
const MainMenu = "res://Scenes/Levels/game.tscn"

# Called when the node enters the scene tree for the first time.
func _ready():
	Events.connect("game_over", onDefeat)
	self.hide()


# Called every frame. 'delta' is the elapsed time since the previous frame.
func _process(delta):
	pass


func _on_main_menu_button_pressed():
	get_tree().reload_current_scene()


func _on_retry_button_pressed():
	Events.restart_level.emit()
	self.visible = false

func onDefeat():
	game_over_sound.play()
	self.show()
