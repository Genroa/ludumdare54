extends Control

var rng = RandomNumberGenerator.new()
var PlayShown = false
var CreditShown = false
var ControlShown = false

var buttonClass = load("res://Scenes/UI/LevelButton.tscn")

@onready var audio_stream_player:AudioStreamPlayer = $AudioStreamPlayer


# Called when the node enters the scene tree for the first time.
func _ready():
	Events.connect("show_main_menu",on_show_main_menu)
	for i in range (1, Utility.level_count + 1):
		var newButt = load("res://Scenes/UI/LevelButton.tscn").instantiate() as LevelButton
		newButt.init(i)

		$PlayPannel/MarginContainer/ScrollContainer/LevelsCont.add_child(newButt)
	
func on_show_main_menu(show):
	self.visible = show
	$MainCamera.enabled = show
	
	if(!show):
		audio_stream_player.stop()


func _on_play_button_pressed():
	if (ControlShown):
		ControlShown = false
		$ControlsScheme.hide()
	if (CreditShown):
		CreditShown = false
		$CreditPannel.hide()
	if (PlayShown):
		PlayShown = false
		$PlayPannel.hide()
	else:
		PlayShown = true
		$PlayPannel.show()


func _on_credits_pressed():
	if (ControlShown):
		ControlShown = false
		$ControlsScheme.hide()
	if (PlayShown):
		PlayShown = false
		$PlayPannel.hide()
	if (CreditShown):
		CreditShown = false
		$CreditPannel.hide()
	else:
		CreditShown = true
		$CreditPannel.show()


func _on_taldius_label_mouse_entered():
	$CreditPannel/VBoxContainer/TaldiusLabel.add_theme_color_override("font_color", Color.YELLOW)
	Utility.RequestShake()


func _on_taldius_label_mouse_exited():
	$CreditPannel/VBoxContainer/TaldiusLabel.add_theme_color_override("font_color", Color.WHITE)


func _on_genroa_label_mouse_entered():
	$CreditPannel/VBoxContainer/GenroaLabel.add_theme_color_override("font_color", Color.YELLOW)
	Utility.RequestShake()


func _on_genroa_label_mouse_exited():
	$CreditPannel/VBoxContainer/GenroaLabel.add_theme_color_override("font_color", Color.WHITE)


func _on_hersif_label_mouse_entered():
	$CreditPannel/VBoxContainer/HersifLabel.add_theme_color_override("font_color", Color.YELLOW)
	Utility.RequestShake()


func _on_hersif_label_mouse_exited():
	$CreditPannel/VBoxContainer/HersifLabel.add_theme_color_override("font_color", Color.WHITE)


func _on_blast_label_mouse_entered():
	$CreditPannel/VBoxContainer/BlastLabel.add_theme_color_override("font_color", Color.YELLOW)
	Utility.RequestShake()


func _on_blast_label_mouse_exited():
	$CreditPannel/VBoxContainer/BlastLabel.add_theme_color_override("font_color", Color.WHITE)


func _on_blauenzag_label_mouse_entered():
	$CreditPannel/VBoxContainer/BlauenzagLabel.add_theme_color_override("font_color", Color.YELLOW)
	Utility.RequestShake()


func _on_blauenzag_label_mouse_exited():
	$CreditPannel/VBoxContainer/BlauenzagLabel.add_theme_color_override("font_color", Color.WHITE)


func _on_quit_button_pressed():
	get_tree().quit()


func _on_control_button_pressed():
	if (CreditShown):
		CreditShown = false
		$CreditPannel.hide()
	if (PlayShown):
		PlayShown = false
		$PlayPannel.hide()
	if (ControlShown):
		ControlShown = false
		$ControlsScheme.hide()
	else:
		ControlShown = true
		$ControlsScheme.show()
