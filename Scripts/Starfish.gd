extends Fish

@export var edible_fish:Enum.FISH_TYPE

@onready var movement = $Movement
@onready var ray_cast_2d = $RayCast2D
@onready var fish_data = $FishData

func _ready():
	ray_cast_2d.connect("eat_fish",_on_eat_fish)
	
func _on_eat_fish(direction: Vector2,eaten_object:Node2D):
	if direction != Vector2.ZERO:
		movement.move(direction)
		
	await get_tree().create_timer(0.1).timeout
	eaten_object.queue_free()

func on_enemy_turn():
	if ray_cast_2d.CheckEdibleFish(edible_fish):
		return

