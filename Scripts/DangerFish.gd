extends Fish

class_name DangerFish

@onready var movement = $Movement
@onready var ray_cast_2d = $RayCast2D

var direction_possibilities: Array[Vector2]

func _ready():
	movement.connect("target_tile_has_collision",_on_target_tile_has_collision)


func _on_target_tile_has_collision():
	go_to_player()

func on_enemy_turn():
	var player_pos:Vector2 = LevelManager.get_current_level().get_spaceship_global_position()
	var target_direction:Vector2 = player_pos - self.global_position
	generate_direction_array(target_direction)
	go_to_player()

func generate_direction_array(target_direction):
	if abs(target_direction.x) >= abs(target_direction.y):
		if(target_direction.y == 0):
			direction_possibilities = [
			Vector2(1,0)* sign(target_direction.x),
			Vector2(0,1),
			Vector2(-1,0)* sign(target_direction.x),
			Vector2(0,-1)]
		else:
			direction_possibilities = [
			Vector2(1,0)* sign(target_direction.x),
			Vector2(0,1)* sign(target_direction.y),
			Vector2(-1,0)* sign(target_direction.x),
			Vector2(0,-1)* sign(target_direction.y)]
		
	else:
		if(target_direction.x == 0):
			direction_possibilities = [
			Vector2(0,1)* sign(target_direction.y),
			Vector2(1,0),
			Vector2(-1,0),
			Vector2(0,-1)* sign(target_direction.y)]
		else:
			direction_possibilities = [
			Vector2(0,1)* sign(target_direction.y),
			Vector2(1,0)* sign(target_direction.x),
			Vector2(-1,0)* sign(target_direction.x),
			Vector2(0,-1)* sign(target_direction.y)]

func go_to_player():
	current_direction = direction_possibilities[0]

	if ray_cast_2d.CheckObstacle(Enum.FISH_TYPE.NONE,current_direction):
		direction_possibilities.remove_at(0)
		if(direction_possibilities.size() == 0):
			current_direction = Vector2.ZERO
			movement.move(current_direction)
			return
		else:
			go_to_player()
			return
	else:
		direction_possibilities.remove_at(0)
		movement.move(current_direction)
		
