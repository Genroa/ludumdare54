class_name ObjectifNode extends Control

@export var fishType: Enum.FISH_TYPE

var Checked = false

# Called when the node enters the scene tree for the first time.
func _ready():
	pass # Replace with function body.

func init(p_fishType):
	$ValidationTex.color = Color.RED
	fishType = p_fishType 
	$FishTex.texture = load(Utility.FishTexture[p_fishType])
	
# Called every frame. 'delta' is the elapsed time since the previous frame.
func _process(delta):
	pass

func setProgress(progress):
	Checked = progress
	if (progress):
		$emptyTex.texture = load("res://Sprites/TEX_checkbox_full.png")
	else:
		$emptyTex.texture = load("res://Sprites/TEX_checkbox_empty.png")

func getProgress():
	return Checked
