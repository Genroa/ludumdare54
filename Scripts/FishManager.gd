extends Node


# Called when the node enters the scene tree for the first time.
func _ready():
	Events.connect("enemy_turn",on_enemy_turn)
	Events.connect("released_fish",on_fish_released)

func on_enemy_turn():
	for fish in get_children():
		if fish != null:
			if fish is Fish:
				fish.on_enemy_turn()
				await get_tree().create_timer(0.05).timeout
	Events.player_turn.emit()

func on_fish_released(fish_data:FishData):
	print("RELEASE FISH")
	
	var fish_resource

	match fish_data.fishType:
		Enum.FISH_TYPE.PLANKTON:
			fish_resource = load("res://Scenes/Fishes/plankton.tscn")
		Enum.FISH_TYPE.MANTA:
			fish_resource = load("res://Scenes/Fishes/manta.tscn")
		Enum.FISH_TYPE.JELLYFISH:
			fish_resource = load("res://Scenes/Fishes/jellyfish.tscn")
		Enum.FISH_TYPE.STARFISH:
			fish_resource = load("res://Scenes/Fishes/starfish.tscn")
		Enum.FISH_TYPE.SHARK:
			pass
		Enum.FISH_TYPE.WHALE:
			pass
		
	var fish = fish_resource.instantiate() as Fish
	add_child(fish)
	fish.global_position = LevelManager.current_level.get_spaceship_global_position()
	fish.current_direction = fish_data.get_last_direction()

	fish.get_node("Movement").on_init_movement()
