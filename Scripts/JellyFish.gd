extends Fish

class_name JellyFish

@export var edible_fish_type:Enum.FISH_TYPE

@onready var movement = $Movement
@onready var ray_cast_2d = $RayCast2D
@onready var fish_data = $FishData
@onready var sprite_2d = $Sprite2D

var is_move_turn = true

func _ready():
	movement.connect("target_tile_has_collision",_on_target_tile_has_collision)
	if current_direction.x > 0 :
		sprite_2d.set_flip_h(true)
	elif current_direction.x < 0:
		sprite_2d.set_flip_h(false)

func _on_target_tile_has_collision():
	current_direction.y *= -1
	
	if movement.get_target_tile_collision(current_direction):
		return
	
	movement.move(current_direction)
	fish_data.set_last_direction(current_direction)

func on_enemy_turn():
	if is_move_turn:
		if ray_cast_2d.CheckEdibleFish(edible_fish_type):
			return
		
		if ray_cast_2d.CheckObstacle(edible_fish_type,current_direction):
			current_direction.y *= -1
		
		movement.move(current_direction)
		fish_data.set_last_direction(current_direction)
		is_move_turn = false
	else:
		movement.move(Vector2.ZERO)
		is_move_turn = true
