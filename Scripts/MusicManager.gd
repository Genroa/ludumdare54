extends Node2D

@onready var player_0:AudioStreamPlayer = $Pressure_0_Player
var current_volume_player_0 = 0
var target_volume_player_0 = 0
@onready var player_1:AudioStreamPlayer = $Pressure_1_Player
var current_volume_player_1 = 0
var target_volume_player_1 = 0
@onready var player_2:AudioStreamPlayer = $Pressure_2_Player
var current_volume_player_2 = 0
var target_volume_player_2 = 0

var mute_volume = 0
var normal_volume = 0.5
var duration = 3.0

# Called when the node enters the scene tree for the first time.
func _ready():
	Events.connect("update_current_pressure",on_pressure_change)
	
	current_volume_player_0 = mute_volume
	current_volume_player_1 = mute_volume
	current_volume_player_2 = mute_volume

func on_pressure_change(pressure:int):
	match (pressure):
		0:
			target_volume_player_0 = normal_volume
			target_volume_player_1 = mute_volume
			target_volume_player_2 = mute_volume
		1:
			target_volume_player_0 = mute_volume
			target_volume_player_1 = normal_volume
			target_volume_player_2 = mute_volume
		2:
			target_volume_player_0 = mute_volume
			target_volume_player_1 = mute_volume
			target_volume_player_2 = normal_volume
		

func _process(delta):
	current_volume_player_0 = move_toward(current_volume_player_0,target_volume_player_0,delta*(1/duration))
	player_0.set_volume_db(linear_to_db(current_volume_player_0))
	current_volume_player_1 = move_toward(current_volume_player_1,target_volume_player_1,delta*(1/duration))
	player_1.set_volume_db(linear_to_db(current_volume_player_1))
	current_volume_player_2 = move_toward(current_volume_player_2,target_volume_player_2,delta*(1/duration))
	player_2.set_volume_db(linear_to_db(current_volume_player_2))


