extends DraggableArea


var storedFishData = null;

# Called when the node enters the scene tree for the first time.
func _ready():
	Events.connect("update_current_tile_object", update_visible_animal)
	Events.connect("manage_inventory", updateHatchAnimation)


func updateHatchAnimation(isManaging):
	if isManaging && $"../windowHatch".animation != "open":
		$"../windowHatch".play("open")
	elif storedFishData == null:
		$"../windowHatch".play("close")
		

func update_visible_animal(fishData):
	storedFishData = fishData
	if(fishData == null):
		texture = null
		$"../windowHatch".play("close")
		return
	
	print(storedFishData.inventorySprite)
	if(storedFishData.inventorySprite != null):
		texture = storedFishData.inventorySprite
		if $"../windowHatch".animation != "open": $"../windowHatch".play("open")

func _get_drag_data(at_position):
	if(storedFishData == null): return
	draggedFishData = storedFishData
	draggedFishData.currentOrientation = 0
	super._get_drag_data(at_position)
	return storedFishData

func _can_drop_data(at_position, data):
	return storedFishData == null

func _drop_data(at_position, data):
	if data == null: return
	storedFishData = data as FishData
	Events.released_fish.emit(storedFishData)
	print("releasing")
	$AudioStreamPlayer.play()
