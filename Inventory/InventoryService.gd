extends Node


@onready var inventoryArray = [
	[null, null, null, null, null, null, null, null],
	[null, null, null, null, null, null, null, null],
	[null, null, null, null, null, null, null, null],
	[null, null, null, null, null, null, null, null],
	[null, null, null, null, null, null, null, null],
]

@onready var fishesArray = []

func reset():
	inventoryArray = [
		[null, null, null, null, null, null, null, null],
		[null, null, null, null, null, null, null, null],
		[null, null, null, null, null, null, null, null],
		[null, null, null, null, null, null, null, null],
		[null, null, null, null, null, null, null, null],
	]
	fishesArray = []

# Called when the node enters the scene tree for the first time.
func _ready():
	print("InventoryService ready")
	Events.start_level.connect(reset)

func rotateShapeClockwiseOnce(originalShape):
	var shape = originalShape.duplicate(true)
	var size = shape.size()
	for i in range(size):
		for j in range(i, size - i - 1):
			var temp = shape[i][j]
			shape[i][j] = shape[size-1-j][i]
			shape[size-1-j][i] = shape[size-1-i][size-1-j]
			shape[size-1-i][size-1-j] = shape[j][size-1-i]
			shape[j][size-1-i] = temp
	
	# Find the smallest offset on rows
	var smallestOffset = 4
	for row in range(shape.size()):
		var rowOffset = 0
		for column in range(shape.size()):
			if shape[row][column] == 0: rowOffset += 1
			else:
				smallestOffset = min(smallestOffset, rowOffset)
				break
#	print("Smallest offset is: ", smallestOffset)
	# Now move everything X cells left
	for row in range(shape.size()):
		for i in range(smallestOffset):
			shape[row].remove_at(0)
			shape[row].append(0)
	return shape
	
	
func computePresenceMask(fishData, at_position):
	var presenceMask = [
		[0, 0, 0, 0, 0, 0, 0, 0],
		[0, 0, 0, 0, 0, 0, 0, 0],
		[0, 0, 0, 0, 0, 0, 0, 0],
		[0, 0, 0, 0, 0, 0, 0, 0],
		[0, 0, 0, 0, 0, 0, 0, 0],
	]
	var indexedPosition = Vector2(
		int(at_position.x/16),
		int(at_position.y/16)
	)
	
	# Build the shape based on the currentOrientation of the fish. Thankfully, their shape is a 5x5 matrix
	var shape = fishData.shape
	for i in range(fishData.currentOrientation):
		shape = rotateShapeClockwiseOnce(shape)
	
	var totalWritten = 0
	# For each row of the shape
	for i in range(shape.size()):
		# For each column of the shape
		if i + indexedPosition.y > 4: continue
		for j in range(shape[0].size()):
			if j + indexedPosition.x > 7: continue
			presenceMask[i+indexedPosition.y][j+indexedPosition.x] = shape[i][j]
			if shape[i][j] == 1: totalWritten += 1
	
#	print("mask:", presenceMask)
#	print("Cells fitting:", totalWritten, " out of ", fishData.fishSize)
	if totalWritten < fishData.fishSize:
		return null
	return presenceMask

func canAddFish(fishData, fishMask, pressureMask):
	# Check every cell of the fishMask. If it's 1, and the cell in the inventory isn't null, 
	# return false. Eventually, return true
	for i in range(fishMask.size()):
		for j in range(fishMask[0].size()):
			if 	fishMask[i][j] == 1 && (pressureMask[i][j] == 1 || (inventoryArray[i][j] != null && inventoryArray[i][j] != fishData)): return false
	return true

func removeFishFromGrid(fishData :FishData):
	for i in range(inventoryArray.size()):
		for j in range(inventoryArray[0].size()):
			if inventoryArray[i][j] == fishData:
				inventoryArray[i][j] = null

func writeFishInGrid(fishData :FishData, fishMask):
	for i in range(fishMask.size()):
		for j in range(fishMask[0].size()):
			if fishMask[i][j] == 1:
				inventoryArray[i][j] = fishData

func registerCaughtFish(fishData :FishData):
	fishesArray = fishesArray.filter(func(f): return f != fishData)
	fishesArray.append(fishData)
	
func addFish(fishData :FishData, fishMask) -> FishData:
	var fishDataCopy = fishData.duplicate()
	# Add the fish to the array of known fishes
	registerCaughtFish(fishDataCopy)
	
	# Write it inside the inventory
	removeFishFromGrid(fishData)
	writeFishInGrid(fishDataCopy, fishMask)
	
	Events.emit_signal("caught_fish", fishData)
	Events.emit_signal("update_inventory", fishesArray)
	return fishDataCopy

func removeFish(fishData :FishData):
	fishesArray = fishesArray.filter(func(f): return f != fishData)
	removeFishFromGrid(fishData)
	Events.emit_signal("update_inventory", fishesArray)	

func getFishAtPosition(position :Vector2):
	return inventoryArray[position.y][position.x]
