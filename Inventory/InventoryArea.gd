extends DraggableArea

var pressureMasks = [
		[
		[0, 0, 0, 0, 0, 0, 0, 0],
		[0, 0, 0, 0, 0, 0, 0, 0],
		[0, 0, 0, 0, 0, 0, 0, 0],
		[0, 0, 0, 0, 0, 0, 0, 0],
		[0, 0, 0, 0, 0, 0, 0, 0],
	],
	[
		[1, 1, 1, 1, 1, 1, 1, 1],
		[1, 0, 0, 0, 0, 0, 0, 1],
		[1, 0, 0, 0, 0, 0, 0, 1],
		[1, 0, 0, 0, 0, 0, 0, 1],
		[1, 0, 0, 0, 0, 0, 0, 1],
	],
	[
		[1, 1, 1, 1, 1, 1, 1, 1],
		[1, 1, 1, 1, 1, 1, 1, 1],
		[1, 1, 0, 0, 0, 0, 1, 1],
		[1, 1, 0, 0, 0, 0, 1, 1],
		[1, 1, 0, 0, 0, 0, 1, 1],
	],
]

@onready var pressureSprites= [
	"res://Sprites/inventory.png",
	"res://Sprites/inventory_p1.png",
	"res://Sprites/inventory_p2.png"
]

@onready var currentPressure = 0
@onready var nextPressure = 0

func findTextureRectForFishData(fishData :FishData):
	for child in get_children():
		var childFishDataNode = child.get_node("FishData") as FishData
		if childFishDataNode == fishData:
			return child

func _get_drag_data(at_position):
	var indexedPosition = Vector2(
		int(at_position.x/16),
		int(at_position.y/16)
	)
	var fishData = InventoryService.getFishAtPosition(indexedPosition)
	draggedFishData = fishData
	if fishData != null: super._get_drag_data(at_position)
	return fishData

func _can_drop_data(at_position, data):
	# If data is null, it means the fish doesn't exist anymore anyway (pressure destroyed it)
	# return true, slots check for null data to ignore these
	if data == null: return true
	# Tip: if this method is called, it's because of at least a partial overlap. No need to check
	# if it's inside or not, it's always at least partially in. But is it totally in? For that, we
	# can check how many spots it wrote in the mask. FishData.fishSize gives us the total count
	
	# Compute the inventory grid mask
	var fishData :FishData = data as FishData
	var presenceMask = InventoryService.computePresenceMask(fishData, at_position)
	if(presenceMask == null):
#		print("Fish doesn't perfectly fit right now. Couldn't drop anyway.")
		Events.emit_signal("update_position_validity", false)
		return false
#	else:
#		print("Fish could maybe fit! Let's check the actual content of the inventory.")
	
	
	# Then submit it to the InventoryService to ask if it can be added
	if !InventoryService.canAddFish(fishData, presenceMask, pressureMasks[currentPressure]):
#		print("No space here according to the InventoryService")
		Events.emit_signal("update_position_validity", false)
		return false
#	print("There's space here, we could accept it!")
	Events.emit_signal("update_position_validity", true)
	return true

func _drop_data(at_position, data):
	if data == null: return
	var fishData :FishData = data as FishData
	var presenceMask = InventoryService.computePresenceMask(fishData, at_position)
	var indexedPosition = Vector2(
		int(at_position.x/16)*16,
		int(at_position.y/16)*16
	)
	
	var textureRect = TextureRect.new()
	textureRect.mouse_filter = Control.MOUSE_FILTER_IGNORE
	textureRect.texture = fishData.inventorySprite
	textureRect.set_position(indexedPosition + fishData.spriteOrientationOffsets[fishData.currentOrientation])
	textureRect.rotation_degrees = fishData.currentOrientation * 90
	InventoryService.removeFish(fishData)
	var fishDataCopy = InventoryService.addFish(fishData, presenceMask)
	add_child(textureRect)
	textureRect.add_child(fishDataCopy)
	$AudioStreamPlayer.play()
	


func _ready():
	Events.connect("released_fish", removeFishFromInventoryArea)
	Events.connect("update_current_pressure", warnUpdateInventoryPressure)
	$Timer.connect("timeout", updateInventoryPressure)
	Events.start_level.connect(reset)
	updateInventoryPressure()

func removeFishFromInventoryArea(fishData :FishData):
	print("Need to remove fish: ", fishData.fishType)
	var child = findTextureRectForFishData(fishData)
	InventoryService.removeFish(fishData)
	child.queue_free()

func warnUpdateInventoryPressure(newPressure):
	if(newPressure > currentPressure):
		nextPressure = newPressure
		$Timer.start()
		$explosion.play()
	else:
		currentPressure = newPressure
		nextPressure = newPressure
		updateInventoryPressure()
		$WarningPressure1.modulate = Color(1, 1, 1, 0)
		$WarningPressure2.modulate = Color(1, 1, 1, 0)

func reset():
	for child in get_children():
		if child is TextureRect && child.get_node("FishData") != null:
			child.queue_free()

func updateInventoryPressure():
	texture = load(pressureSprites[nextPressure])
	if nextPressure > currentPressure:
		Utility.RequestShake()
		$WarningPressure1.modulate = Color(1, 1, 1, 0)
		$WarningPressure2.modulate = Color(1, 1, 1, 0)
	if nextPressure < currentPressure:
		$remonte.play()
		print('salut fdp')
		$explosion.stop()
	else:
		$explosion.stop()
	
	currentPressure = nextPressure
	print("currentPressure=", currentPressure)
	$Timer.stop()
	
	var currentPressureMask = pressureMasks[currentPressure]
	for row in range(currentPressureMask.size()):
		for column in range(currentPressureMask[row].size()):
			if currentPressureMask[row][column] ==0: continue
			var fishData = InventoryService.getFishAtPosition(Vector2(column, row))
			if fishData != null:
				removeFishFromInventoryArea(fishData)


func _process(delta):
	if currentPressure < nextPressure:
		if nextPressure >= 1:
			var c = $WarningPressure1.modulate
			$WarningPressure1.modulate = Color(c.r, c.g, c.b, c.a + delta * 0.3)
		
		if nextPressure >= 2:
			var c = $WarningPressure2.modulate
			$WarningPressure2.modulate = Color(c.r, c.g, c.b, c.a + delta * 0.3)
