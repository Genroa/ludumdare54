class_name DraggableArea extends TextureRect

@export_category("Fish Data")
@export var draggedFishData :FishData = null
@export var draggingBounds :TextureRect = null

func createGhost(fishData :FishData, at_position :Vector2):
	var ghost = load("res://Inventory/Ghost.tscn").instantiate()
	ghost.fishData = fishData
	ghost.set_texture(draggedFishData.inventorySprite)
	ghost.draggingBounds = draggingBounds
	return ghost

func _get_drag_data(at_position):
	var ghost = createGhost(draggedFishData, at_position)
	set_drag_preview(ghost)
	Events.emit_signal("manage_inventory", true)
	return {}

func _can_drop_data(at_position, data):
	return false

func _drop_data(at_position, data):
	Events.emit_signal("manage_inventory", false)
	pass
