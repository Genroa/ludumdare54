class_name Ghost extends Control

@export var fishData :FishData = null
@export var draggingBounds: TextureRect = null
var viewportRect: Rect2 = Rect2();
var draggingRect: Rect2 = Rect2();
var hoveringPosition: Vector2 = Vector2();

func clamp(minValue, value, maxValue) -> int:
	return int(max(minValue, min(value, maxValue)))


# Called when the node enters the scene tree for the first time.
func _ready():
	viewportRect = get_viewport_rect()
	draggingRect = draggingBounds.get_rect()
	Events.connect("update_position_validity", updatePositionValidity)

func updatePositionValidity(isValid):
	if isValid: $TextureRect.modulate = Color(1, 1, 1, 0.7)
	else: $TextureRect.modulate = Color(1, 0.8, 0.8, 0.3)

# Called every frame. 'delta' is the elapsed time since the previous frame.
func _process(delta):
	var finalPosition = Vector2()
	var mousePosition = get_global_mouse_position()
	var clampedMousePosition = Vector2(
		clamp(draggingRect.position.x, mousePosition.x, draggingRect.position.x+draggingRect.size.x-15), 
		clamp(draggingRect.position.y, mousePosition.y, draggingRect.position.y+draggingRect.size.y-15)
	)
	
	# If we're inside the "inventory" part, snap the position
	if(clampedMousePosition.y < 80):
		finalPosition = Vector2(
			int(clampedMousePosition.x - int(clampedMousePosition.x)%16),
			int(clampedMousePosition.y - int(clampedMousePosition.y)%16)
		)
	else:
		finalPosition = clampedMousePosition
	set_position(finalPosition)
	if(hoveringPosition.x != finalPosition.x || hoveringPosition.y != finalPosition.y):
		hoveringPosition = finalPosition


func set_texture(newTexture :Texture2D):
	$TextureRect.texture = newTexture
	$TextureRect.set_size(newTexture.get_size())
	updateTextureOrigin()
#	$TextureRect.expand_mode = $TextureRect.EXPAND_FIT_WIDTH_PROPORTIONAL
#	$TextureRect.stretch_mode = $TextureRect.EXPAND_KEEP_SIZE

func _exit_tree():
	Events.emit_signal("manage_inventory", false)

func _unhandled_input(event):
	if event.is_action_pressed("rright"):
		fishData.currentOrientation = (fishData.currentOrientation+1)%4
		updateTextureOrigin()
		$AudioStreamPlayer.play()
	elif event.is_action_pressed("rleft"):
		fishData.currentOrientation = fishData.currentOrientation-1
		if fishData.currentOrientation < 0: fishData.currentOrientation = 3
		updateTextureOrigin()
		$AudioStreamPlayer.play()

func updateTextureOrigin():
	$TextureRect.set_position(fishData.spriteOrientationOffsets[fishData.currentOrientation])
	$TextureRect.rotation_degrees = 90 * fishData.currentOrientation
