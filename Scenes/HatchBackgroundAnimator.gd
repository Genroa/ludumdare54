extends AnimatedSprite2D

@onready var hatchAnimations : Array = [
	"res://Sprites/hatch_background_0.tres",
	"res://Sprites/hatch_background_1.tres",
	"res://Sprites/hatch_background_2.tres"
]

# Called when the node enters the scene tree for the first time.
func _ready():
	Events.update_current_pressure.connect(updateHatchAnimation)

func updateHatchAnimation(newPressure):
	var res = load(hatchAnimations[newPressure]) as SpriteFrames
	sprite_frames = res
	play("default")
