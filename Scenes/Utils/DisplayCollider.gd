extends Area2D

@export var target :Node

# Called when the node enters the scene tree for the first time.
func _ready():
	connect("area_entered", displayTarget) # Replace with function body.
	connect("area_exited", hideTarget)

# Called every frame. 'delta' is the elapsed time since the previous frame.
func _process(delta):
	pass

func displayTarget(area :Area2D):
	if area.name != "SpaceshipArea2D": return
	if target == null: return
	target.show()

func hideTarget(area :Area2D):
	if area.name != "SpaceshipArea2D": return
	if target == null: return
	target.hide()
