extends Node

@export var fish_to_catch :Array[Enum.FISH_TYPE]

func _ready():
	Events.connect("update_inventory",on_update_inventory)

func on_update_inventory(fish_data_array):
	return
	#dirtybackup
	var counter = 0
	print("new fish array:", fish_data_array)
	for fish in fish_data_array:
		if fish_to_catch.has(fish.fishType):
			counter += 1
			
	if counter >= fish_to_catch.size():
		Events.finish_level.emit()
